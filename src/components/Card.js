import React from 'react';
import { NavLink } from 'react-router-dom';

const Card = ({ galerie }) => {
    return (
        <div className='gallery__card'>
            <NavLink to={`/${galerie.id}`}>
                <img src={galerie.cover} alt={galerie.title} className='gallery__card--image' />
            </NavLink>
            <ul className='gallery__card--text'>
                <li className='title'>{galerie.title}</li>
                <li className='location'>{galerie.location}</li>
            </ul>
        </div>
    );
};

export default Card;