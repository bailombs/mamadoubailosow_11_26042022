import React from 'react';
import { NavLink } from 'react-router-dom';


const Navigation = () => {
    return (
        <ul className='header__navigation'>
            <li className='header__navigation--link'>
                <NavLink to="/" className={(nav) => (nav.isActive ? "nav-active" : "")}>Accueil</NavLink>
            </li>
            <li className='header__navigation--link'>
                <NavLink to="/about" className={(nav) => (nav.isActive ? "nav-active" : "")} >A Propos</NavLink>
            </li>
        </ul>
    );
};

export default Navigation;