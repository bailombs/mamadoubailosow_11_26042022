import {FaStar} from 'react-icons/fa'
import Tag from './Tag';
const Profile = ({housing}) => {
   

    const ranges =[1,2,3,4,5]
    const numberRating = parseInt(housing.rating)
    
    return (
        <section className='profile'>
            <ul className='profile__title'>
                <li className='profile__title--title'>{housing.title}</li>
                <li className='profile__title--location'>{housing.location}</li>
                <li className='profile__title--tag'>{
                   housing.tags.map((tag)=> <Tag key={tag} tag={tag} /> ) 
                }</li>
            </ul>
            <ul className='profile__host'>
                <li className='profile__host--name'>{housing.host.name}</li>
                <li className='profile__host--picture'>
                    <img src={housing.host.picture} alt={housing.host.name} />
                </li>
                <li className='profile__host--star'>{ranges.map(                    
                    (range)=>                 
                        range < numberRating ?
                        <FaStar key={range} className='orangeColor'/> : <FaStar key={range} className='grayColor'/>)                  
                    }
                </li>                 
            </ul>
            
        </section>
    );
};

export default Profile;