import React from 'react';
import logo from '../assets/images/LOGO.png'
import { Link } from 'react-router-dom';
const Logo = () => {
    return (
        <div className='header__logo' >
            <Link to="/">
                <img src={logo} alt="logo" />
            </Link>

        </div>
    );
};

export default Logo;