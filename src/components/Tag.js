import React from 'react';

const Tag = ({tag}) => {
    return (
        <span>{tag}</span>
    );
};

export default Tag;