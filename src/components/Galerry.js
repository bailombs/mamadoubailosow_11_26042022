import React from 'react';
import data from '../datas/data.json';
import Card from './Card';
const Galerry = () => {
    return (
        <div className='gallery'>{
            data.map((galerie) => (
                <Card key={galerie.id} galerie={galerie} />
            ))
        }</div>
    );
};

export default Galerry;