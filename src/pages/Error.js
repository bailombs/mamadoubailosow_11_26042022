import React from 'react';
import { NavLink } from 'react-router-dom';
import Header from '../components/Header';

const Error = () => {
    return (
        <React.Fragment>
            <Header />
            <h1 className='numberError'> 404 </h1>
            <p className='messageError'>Oups ! La page que vous demandez n'existe pas</p>

            <p className='back'>
                <NavLink to="/">Retourner sur la page d'Accueil</NavLink>
            </p>
        </React.Fragment>
    );
};

export default Error;