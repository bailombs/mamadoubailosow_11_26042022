import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import DropDownMenu from '../components/DropDownMenu';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Profile from '../components/Profile';
import Slideshow from '../components/Slideshow';
import data from '../datas/data.json';

const Logement = () => {
    const params = useParams()
    const navigate = useNavigate()
    const [housing, setHousing] = useState()

    useEffect(() => {
        const currentHousing = data.find(logement => logement.id === params.id)
        if (!currentHousing) {

            navigate("/error")
        } else {
            setHousing(currentHousing)
        }

    }, [params, navigate])

    if (housing) {
        return (
            <React.Fragment>
                <Header />
                <Slideshow housing={housing} />
                <Profile housing={housing} />
                <section className='containerDropdown'>
                    <DropDownMenu dropDownType='array' dropdown={housing.equipments} nameTitle={'Equipement'} />
                    <DropDownMenu dropDownType='text' dropdown={housing.description} nameTitle={'Description'} />
                </section>
                <Footer />
            </React.Fragment>
        )


    }
    return null

};

export default Logement;